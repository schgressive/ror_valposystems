Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  mount RailsAdmin::Engine => '/rails_admin', as: 'rails_admin'
  
  resources :jobs


  get 'welcome/index'

  get '/graphs' => "clients#graphs"

  get '/postulacion/:job_id' => "postulacion#show"
  post '/postulacion' => "postulacion#create"


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
end
