module WelcomeHelper

	def pluralizar numero
		if numero == 0
			mensaje = "0 solicitudes"
		elsif numero == 1
			mensaje = "1 solicitud"
		elsif numero > 1
			mensaje = "#{numero} solicitudes"			
		end
		mensaje
	end
end
