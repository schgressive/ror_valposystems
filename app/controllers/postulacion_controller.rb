class PostulacionController < ApplicationController
	before_action :authenticate_user!

	def show
		@jobs = Job.all.map { |u| [u.name, u.id] }
		@users = User.all.map { |u| [u.name, u.id] }
		@job = params[:job_id]
		@user = current_user.id
	end

	def create
		job_app = JobApplication.create(job_params)
		if job_app.errors.messages.empty?
			flash[:notice] = "Listo, tu postulación ha sido enviada"
		else
			flash[:notice] = job_app.errors.messages.to_s
		end
		redirect_to "/postulacion/#{job_params[:job_id]}"
	end

	private

	def job_params
		params.permit(:job_id, :user_id, :motivation)
	end
end
