class ClientsController < ApplicationController

	def graphs
		@solicitudes_por_empresa = solicitudes
		render "graphs"
	end

	private

	def solicitudes
		arr = []
		Client.includes(:job_applications).all.each do |client|
			count = client.job_applications.count
			arr	<< [client.name, count] if count > 0
		end
		return arr.sort.first(5)
	end

end
