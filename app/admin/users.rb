ActiveAdmin.register User do

  menu priority: 1, :if => proc {false}

  permit_params :email, :password, :password_confirmation

  # LIST
  index do
    selectable_column
    id_column
    column :email do |user|
      link_to(user.email, "users/#{user.id}")
    end
    column :name
    column "Creado el", :created_at
  end

  # SHOW
  show do
    attributes_table do
      row :email do |user|
        link_to(user.email, "users/#{user.id}")
      end
      row :name
      row "Creado el", :created_at
    end
  end

  # Filtros
  filter :email
  filter :name


  # CREATE OR UPDATE
  form do |f|
    f.inputs do
      f.input :client
      f.input :email
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
