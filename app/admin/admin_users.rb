ActiveAdmin.register AdminUser do

  menu priority: 1, :if => proc {false}

  permit_params :email, :password, :password_confirmation, :client

  # SHOW/LIST
  index do
    selectable_column
    id_column
    column :email do |admin_user|
      link_to(admin_user.email, "admin_users/#{admin_user.id}")
    end
    column :client
    column "Nro de veces logeado", :sign_in_count
    column "Creado el", :created_at
    # actions
  end

  # Filtros
  filter :email
  filter :client


  # CREATE OR UPDATE
  form do |f|
    f.inputs do
      f.input :client
      f.input :email
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
