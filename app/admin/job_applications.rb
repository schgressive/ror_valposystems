ActiveAdmin.register JobApplication do

  scope("Todos los del Cliente", default: true) { |scope| scope.of_client(current_admin_user.client) }

  permit_params :motivation, :user_id, :client_id

  # SHOW/LIST
  index do
    selectable_column
    id_column
    column :user
    column :job
    column :motivation
    # actions
  end

  # Filtros
  filter :user
  filter :job


  # CREATE OR UPDATE
  form do |f|
    f.inputs do
		f.input :user
		f.input :job
		f.input :motivation
    end
    f.actions
  end

end
