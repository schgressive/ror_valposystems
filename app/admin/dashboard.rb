ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        span "Bienvenido! Admin de la Empresa"
        strong current_admin_user.client.name if current_admin_user.client
      end
    end

    # div class: "graphs" do
    #     render 'some_partial', { post: post }
    # end
  end 
end
