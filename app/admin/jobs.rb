ActiveAdmin.register Job do

  scope("Todos los del Cliente", default: true) { |scope| Job.where(client: current_admin_user.client) }

  permit_params :name, :description, :job_type

  # SHOW/LIST
  index do
    selectable_column
    id_column
    column :name
    column :description
    column :job_type
    column "Creado el", :created_at
    # actions
  end

  # Filtros
  filter :name
  filter :job_type


  # CREATE OR UPDATE
  form do |f|
    f.inputs do
		f.input :name
		f.input :description
		f.input :job_type
    end
    f.actions
  end

end
