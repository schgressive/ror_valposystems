class Client < ApplicationRecord
	validates :name, presence: true, length: { in: 2..20 }

	has_many :jobs, :inverse_of => :client, dependent: :destroy
	has_many :admin_users, :inverse_of => :client, dependent: :destroy
	has_many :job_applications, :through => :jobs

end
