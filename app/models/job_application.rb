class JobApplication < ApplicationRecord
	belongs_to :job, :inverse_of => :job_applications
	belongs_to :user, :inverse_of => :job_applications
	validates :motivation, presence: true

	# scope :of_client, (client) -> { self.joins(:job).where(job: {client: client}) }

	def self.of_client (client)
		self.joins(:job).where(jobs: {client: client})
	end

	def client_name
		if self.job
			self.job.client.name
		end
	end

end
