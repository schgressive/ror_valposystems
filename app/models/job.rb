class Job < ApplicationRecord
	validates :name, presence: true, length: { in: 4..20 }
	validates :description, presence: true

	has_many :job_applications, :inverse_of => :job, dependent: :destroy
	belongs_to :client, :inverse_of => :jobs

end
