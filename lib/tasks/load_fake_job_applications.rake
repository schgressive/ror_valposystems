## 
## rake load:fake_job_applications
##

namespace :load do
	desc "Creo solicitudes falsas para desarrollo"
	task :fake_job_applications => :environment do 


		JobApplication.destroy_all

		100.times do 
			JobApplication.create(
				job: Job.all.sample,
				user: User.all.sample, 
				motivation: Faker::Lorem.paragraph(2, false, 4),
				created_at: Faker::Date.between(20.days.ago, Date.today)
			)
		end

	end
	
end