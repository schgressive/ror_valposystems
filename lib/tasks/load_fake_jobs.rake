## 
## rake load:fake_jobs
##
namespace :load do
	desc "Creo Trabajos falsos para desarrollo"
	task fake_jobs: :environment do

		Job.destroy_all

		100.times do 
			client = Client.all.sample
			Job.create(
				name: Faker::Job.title, 
				job_type: ['Jornada Completa', 'PartTime', 'FindeSemana'].sample, 
				description: Faker::Lorem.paragraph(2, false, 4),
				client: client
			)
		end

	end
end
