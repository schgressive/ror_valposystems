## 
## rake load:fake_clients
##

namespace :load do
	desc "Creo clientes falsos para desarrollo"
	task :fake_clients => :environment do 


		Client.destroy_all

		100.times do 
			Client.create(
				name: Faker::Company.name, 
				rut: Faker::Number.between(700000000,760000000),
				address: Faker::Address.full_address,
				phone: Faker::PhoneNumber.cell_phone,
				created_at: Faker::Date.between(20.days.ago, Date.today)
			)
		end

	end
	
end