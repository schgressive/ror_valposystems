# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
u = User.find_or_create_by name: "Usuario Test", email: 'test@test.com'
u.update password: 'testtest', confirmed_at: DateTime.now

client = Client.find_or_create_by name: "Empresa SA", rut: "12312312-1"
au = AdminUser.find_or_create_by(client: client, email: 'admin@example.com')
au.update password: 'password', password_confirmation: 'password'