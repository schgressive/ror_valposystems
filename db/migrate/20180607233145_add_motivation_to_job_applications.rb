class AddMotivationToJobApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :job_applications, :motivation, :string
  end
end
