class ChangeClientInJobs < ActiveRecord::Migration[5.2]
  def change
  	remove_column :jobs, :client, :string
  	add_column :jobs, :client_id, :integer
  end
end
