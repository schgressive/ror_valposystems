class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :name 
      t.string :description
      t.integer :uid
      t.string :client

      t.timestamps
    end
  end
end
