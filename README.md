# JobBoard

## Requisitos
* RVM (`\curl -sSL https://get.rvm.io | bash -s stable`)
* Ruby (2.4.0)
* Postgres

## Instalar Dependencias

`rvm use 2.4.0@valposystems --create`
`gem install bundler`
`bundle install`

## Crear BD y migraciones

`rake db:create`
`rake db:migrate`

## Levantar Servidor de Desarrollo
`rails s`

## Levantar Consola de Rails
`rails c`